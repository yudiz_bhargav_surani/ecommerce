const { Sequelize, DataTypes, Transaction } = require("sequelize");

const dbConnection = new Sequelize("test", "root", "", {
  host: "localhost",
  dialect: "mysql",
  isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ,
});

try {
  dbConnection.authenticate();
  console.log("Database connected");
} catch (error) {
  console.log("Database error " + error);
}

const db = {};
db.Sequelize = Sequelize;
db.dbConnection = dbConnection;

module.exports = db;
