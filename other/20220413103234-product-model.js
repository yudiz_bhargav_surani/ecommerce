"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Product", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iSellerId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Seller",
          key: "id",
        },
      },
      iCatogoryId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Category",
          key: "id",
        },
      },
      sName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sDescription: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      nPrice: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
      eStatus: {
        type: DataTypes.ENUM("InStock", "OutOfStock"),
        defaultValue: "InStock",
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Product");
  },
};
