"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ReviewProduct", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iProductId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Product",
          key: "id",
        },
      },
      iCustomerId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Customer",
          key: "id",
        },
      },
      nRating: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
      sDescription: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ReviewProduct");
  },
};
