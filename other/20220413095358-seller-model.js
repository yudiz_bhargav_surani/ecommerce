"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Seller", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      sFirstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sLastName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sEmail: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      nMobile: {
        type: DataTypes.BIGINT,
        allowNull: false,
        unique: true,
      },
      sPassword: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      bIsActive: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Seller");
  },
};
