"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Invoice", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iSellerId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Seller",
          key: "id",
        },
      },
      iTransationId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Transaction",
          key: "id",
        },
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Invoice");
  },
};
