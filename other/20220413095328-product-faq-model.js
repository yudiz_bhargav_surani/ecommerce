"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ProductFAQ", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iProductId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Product",
          key: "id",
        },
      },
      sQuestion: { type: DataTypes.STRING, allowNull: false },
      sAnswer: { type: DataTypes.STRING, allowNull: false },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ProductFAQ");
  },
};
