const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
const db = require("../db/connection");
class Address extends Model {}

Address.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iCustomerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Customer",
        key: "id",
      },
    },
    sFullName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sAddressLine1: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sAddressLine2: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sDist: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sCity: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sCountry: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    nPinCode: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    sType: {
      type: DataTypes.ENUM("Home", "Work", "Office"),
      defaultValue: "Home",
      allowNull: false,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Address",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Address;
