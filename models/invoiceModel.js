const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");

class Invoice extends Model {}

Invoice.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iSellerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Seller",
        key: "id",
      },
    },
    iTransactionId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Transaction",
        key: "id",
      },
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Invoice",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Invoice;
