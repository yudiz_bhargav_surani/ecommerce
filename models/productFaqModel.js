const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class ProductFAQ extends Model {}

ProductFAQ.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iProductId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Product",
        key: "id",
      },
    },
    sQuestion: { type: DataTypes.STRING, allowNull: false },
    sAnswer: { type: DataTypes.STRING, allowNull: false },
  },
  {
    sequelize: dbConnection,
    modelName: "ProductFAQ",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = ProductFAQ;
