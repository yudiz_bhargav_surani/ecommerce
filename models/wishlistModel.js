const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Wishlist extends Model {}

Wishlist.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iCustomerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Customer",
        key: "id",
      },
    },

    iProductId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Product",
        key: "id",
      },
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Wishlist",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Wishlist;
