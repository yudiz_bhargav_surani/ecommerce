const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");

class Product extends Model {}

Product.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iSellerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Seller",
        key: "id",
      },
    },
    iCategoryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Category",
        key: "id",
      },
    },
    sName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sDescription: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    nPrice: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    eStatus: {
      type: DataTypes.ENUM("InStock", "OutOfStock"),
      defaultValue: "InStock",
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Product",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Product;
