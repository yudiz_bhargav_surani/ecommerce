const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Order extends Model {}

Order.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iCustomerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Customer",
        key: "id",
      },
    },
    iProductId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Product",
        key: "id",
      },
    },
    nQuantity: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    nAmount: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    eStatus: {
      type: DataTypes.ENUM("Pending", "ReadyToShip", "Shipped", "Delivered"),
      defaultValue: "Pending",
    },
    dActivityDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Order",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Order;
