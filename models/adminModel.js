const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Admin extends Model {}

Admin.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    sFirstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sLastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sEmail: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    nMobile: {
      type: DataTypes.BIGINT,
      allowNull: false,
      unique: true,
    },
    sPassword: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Admin",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Admin;
