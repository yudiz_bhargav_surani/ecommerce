const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");

class ReviewProduct extends Model {}

ReviewProduct.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iProductId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Product",
        key: "id",
      },
    },
    iCustomerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Customer",
        key: "id",
      },
    },
    nRating: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    sDescription: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "ReviewProduct",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = ReviewProduct;
