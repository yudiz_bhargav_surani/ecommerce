const Address = require("./addressModel");
const Admin = require("./adminModel");
const Cart = require("./cartModel");
const Category = require("./categoryModel");
const Customer = require("./customerModel");
const Invoice = require("./invoiceModel");
const Order = require("./orderModel");
const ProductFAQ = require("./productFaqModel");
const Product = require("./productModel");
const ReviewProduct = require("./reviewProductModel");
const Seller = require("./sellerModel");
const Transaction = require("./transactionModel");
const Wishlist = require("./wishlistModel");

Customer.hasMany(Address, { foreignKey: "iCustomerId" });
Customer.hasMany(Order, { foreignKey: "iCustomerId" });
Customer.hasMany(Cart, { foreignKey: "iCustomerId" });
Customer.hasMany(Transaction, { foreignKey: "iCustomerId" });
Customer.hasMany(ReviewProduct, { foreignKey: "iCustomerId" });
Customer.hasMany(Wishlist, { foreignKey: "iCustomerId" });
Address.belongsTo(Customer, { foreignKey: "iCustomerId" });
Order.belongsTo(Customer, { foreignKey: "iCustomerId" });
Cart.belongsTo(Customer, { foreignKey: "iCustomerId" });
Transaction.belongsTo(Customer, { foreignKey: "iCustomerId" });
ReviewProduct.belongsTo(Customer, { foreignKey: "iCustomerId" });
Wishlist.belongsTo(Customer, { foreignKey: "iCustomerId" });

Seller.hasMany(Product, { foreignKey: "iSellerId" });
Seller.hasMany(Invoice, { foreignKey: "iSellerId" });
Product.belongsTo(Seller, { foreignKey: "iSellerId" });
Invoice.belongsTo(Seller, { foreignKey: "iSellerId" });

Product.hasMany(Cart, { foreignKey: "iProductId" });
Product.hasMany(Order, { foreignKey: "iProductId" });
Product.hasMany(ReviewProduct, { foreignKey: "iProductId" });
Product.hasMany(ProductFAQ, { foreignKey: "iProductId" });
Product.hasMany(Wishlist, { foreignKey: "iProductId" });
Cart.hasMany(Product, { foreignKey: "iProductId" });
Order.hasMany(Product, { foreignKey: "iProductId" });
ReviewProduct.hasMany(Product, { foreignKey: "iProductId" });
ProductFAQ.hasMany(Product, { foreignKey: "iProductId" });
Wishlist.hasMany(Product, { foreignKey: "iProductId" });

Category.hasMany(Product, { foreignKey: "iCategoryId" });
Product.belongsTo(Category, { foreignKey: "iCategoryId" });

Order.hasOne(Transaction, { foreignKey: "iOrderId" });
Transaction.belongsTo(Order, { foreignKey: "iOrderId" });

Transaction.hasOne(Invoice, { foreignKey: "iTransactionId" });
Invoice.belongsTo(Transaction, { foreignKey: "iTransactionId" });

module.exports = {
  Customer,
  Address,
  Admin,
  Cart,
  Category,
  Invoice,
  Order,
  ProductFAQ,
  Product,
  ReviewProduct,
  Seller,
  Transaction,
  Wishlist,
};
