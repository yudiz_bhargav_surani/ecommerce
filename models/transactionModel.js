const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Transaction extends Model {}

Transaction.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iOrderId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Order",
        key: "id",
      },
    },
    iCustomerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Customer",
        key: "id",
      },
    },
    nAmount: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    sPaymentMethod: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    eStatus: {
      type: DataTypes.ENUM("Success", "Failed"),
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Transaction",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Transaction;
