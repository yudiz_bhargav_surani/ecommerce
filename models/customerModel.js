const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Customer extends Model {}

Customer.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    sFirstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sLastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sEmail: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    nMobile: {
      type: DataTypes.BIGINT,
      allowNull: false,
      unique: true,
    },
    sPassword: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bIsActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Customer",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

// Customer.beforeCreate((user) => {
//   user.sFirstName = "Bhargav";
// });

module.exports = Customer;
