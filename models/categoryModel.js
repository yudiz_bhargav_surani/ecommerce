const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Category extends Model {}

Category.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    sName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sDescription: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Category",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Category;
