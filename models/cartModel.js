const { DataTypes, Model } = require("sequelize");
const { dbConnection } = require("../db/connection");
class Cart extends Model {}

Cart.init(
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
    },
    iProductId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Product",
        key: "id",
      },
    },
    iCustomerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: "Customer",
        key: "id",
      },
    },
    nQuantity: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    nAmount: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
  },
  {
    sequelize: dbConnection,
    modelName: "Cart",
    freezeTableName: true,
    timestamps: true,
    createdAt: "dCreatedAt",
    updatedAt: "dUpdatedAt",
  }
);

module.exports = Cart;
