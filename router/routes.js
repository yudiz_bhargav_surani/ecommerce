const router = require("express").Router();

const customer = require("./customer");

router.use("/customer", customer);

router.all("*", (req, res) => {
  try {
    return res.status(400).json({ message: "Route not found!" });
  } catch (error) {
    return res.status(404).json({ message: error.message });
  }
});

module.exports = router;
