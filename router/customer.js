const router = require("express").Router();
const controller = require("../controllers/customer");

router.post("/signup", controller.signup);
router.post("/changeProfile", controller.changeProfile);
router.get("/profile", controller.profile);
router.get("/transaction", controller.trans);
router.get("/transaction2", controller.trans2);

module.exports = router;
