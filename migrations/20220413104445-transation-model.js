"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Transaction", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iOrderId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Order",
          key: "id",
        },
      },
      iCustomerId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Customer",
          key: "id",
        },
      },
      nAmount: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
      sPaymentMethod: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      eStatus: {
        type: DataTypes.ENUM("Success", "Failed"),
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Transaction");
  },
};
