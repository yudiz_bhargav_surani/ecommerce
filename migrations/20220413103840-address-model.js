"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Address", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iCustomerId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Customer",
          key: "id",
        },
      },
      sFullName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sAddressLine1: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sAddressLine2: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sDist: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sCity: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sCountry: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      nPinCode: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
      sType: {
        type: DataTypes.ENUM("Home", "Work", "Office"),
        defaultValue: "Home",
        allowNull: false,
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Address");
  },
};
