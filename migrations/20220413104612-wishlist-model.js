"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Wishlist", {
      id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      iCustomerId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Customer",
          key: "id",
        },
      },

      iProductId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "Product",
          key: "id",
        },
      },
      dCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      dUpdatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Wishlist");
  },
};
