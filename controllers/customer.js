const controller = {};

const { Op, Transaction, where, QueryTypes } = require("sequelize");
const { max } = require("../models/addressModel");

const { Address, Customer, Wishlist } = require("../models/Associations");
controller.signup = async (req, res) => {
  const { sFirstName, sLastName, sEmail, nMobile, sPassword } = req.body;
  try {
    const result = await Customer.create({
      sFirstName,
      sLastName,
      sEmail,
      nMobile,
      sPassword,
    });
    console.log(result.dataValues);
    return res.status(201).json({ message: "Successfully registers" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

(async () => {})();

controller.profile = async (req, res) => {
  const { id } = req.body;
  try {
    // const result = await Customer.findAndCountAll({
    //   where: {
    //     id: { [Op.in]: [1, 2, 3] },
    //   },
    //   offset: 0,
    //   limit: 2,
    // });

    const result = await Customer.findAndCountAll({
      // where: { id: 1 },
      include: [{ model: Address, required: true }, { model: Wishlist }],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

controller.changeProfile = async (req, res) => {
  const { sFirstName, sLastName, sEmail, nMobile, sPassword } = req.body;
  try {
    const result = await Customer.update(
      {
        sFirstName: "Pranav",
      },
      {
        where: {
          id: 1,
        },
      }
    );
    // console.log(result.dataValues);
    return res.status(201).json({ message: "Successfully Updated" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

controller.trans = async (req, res) => {
  const t1 = await Customer.sequelize.transaction();
  const t2 = await Customer.sequelize.transaction();
  try {
    const result3 = await Customer.findAll({ where: { id: 1 }, attributes: ["nMobile"], transaction: t1 });
    console.log("transaction 1 find", result3[0].dataValues);

    const result = await Customer.findAll({ where: { id: 1 }, attributes: ["nMobile"], transaction: t2 });
    console.log("transaction 2 find", result[0].dataValues);

    const result2 = await Customer.increment({ nMobile: 5 }, { where: { id: 1 }, transaction: t1 });
    console.log("transaction 1 update", result2);

    await t1.commit();

    // setTimeout(async () => {
    const result4 = await Customer.findAll({ where: { id: 1 }, attributes: ["nMobile"], transaction: t2 });
    console.log("transaction 2 find after commit", result4[0].dataValues);
    // }, 5000);
    // setTimeout(async () => {
    await t2.commit();
    // }, 10000);
  } catch (error) {
    console.log("error");

    await t1.rollback();
    await t2.rollback();
  }
};

controller.trans2 = async (req, res) => {
  const t1 = await Customer.sequelize.transaction();
  try {
    const result3 = await Customer.findAll({ where: { id: 1 }, attributes: ["nMobile"], transaction: t1 });
    console.log("transaction 1 find", result3[0].dataValues);

    const result2 = await Customer.increment({ nMobile: 5 }, { where: { id: 1 }, transaction: t1 });
    console.log("transaction 1 update", result2);

    setTimeout(async () => {
      await t1.commit();
    }, 5000);
  } catch (error) {
    console.log("error");
    setTimeout(async () => {
      await t1.rollback();
    }, 5000);
  }
};
module.exports = controller;
