const Customer = require("../models/customerModel");
const { Transaction } = require("sequelize");

const transaction = await Transaction();

try {
  const result = await Customer.create(
    {
      sFirstName: "Meet",
      sLastName: "Shah",
      sEmail: "meet@gmail.com",
      nMobile: 123456780,
      sPassword: "meet",
    },
    { transaction }
  );
  console.log(result);
  const result2 = await Customer.create(
    {
      sFirstName: "Hiren",
      sLastName: "Patodiya",
      sEmail: "hiren@gmail.com",
      nMobile: 123456780,
      sPassword: "hiren",
    },
    { transaction }
  );
  console.log(result2);

  await transaction.commit();
} catch (error) {
  console.log("error");
  await transaction.rollback();
}
